#!/usr/bin/python3

import socket, sys, os, time, pty

port = int(sys.argv[1])
socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.bind(('127.0.0.1', port))
socket.listen(2)
print("Attente d'un client...")

#####################################
# Fonctions des commandes distantes #
#####################################

def rls_command():
  fileForReturn = os.open("/opt/resultCom.res",os.O_RDWR | os.O_CREAT)
  pFils = os.fork()
  if(pFils):
    os.waitpid(pFils, 0)
  else:
    os.dup2(fileForReturn, pty.STDOUT_FILENO)
    os.execlp('ls','ls','-a')
  file_result = open("/opt/resultCom.res","r")
  result = file_result.read()
  os.remove("/opt/resultCom.res")
  print("reponse rls envoyée")
  return result

def rpwd_command():
  fileForReturn = os.open("/opt/resultCom.res",os.O_RDWR | os.O_CREAT)
  pFils = os.fork()
  if(pFils):
    os.waitpid(pFils, 0)
  else:
    os.dup2(fileForReturn, pty.STDOUT_FILENO)
    os.execlp('pwd','pwd',)
  file_result = open("/opt/resultCom.res","r")
  result = file_result.read()
  os.remove("/opt/resultCom.res")
  print("reponse rpwd envoyée")
  return result

def rcd_command(path):
  pFils = os.fork()
  msgToReturn = ""
  if(pFils):
    os.waitpid(pFils, 0)
  else:
    try:
      os.chdir(path)
      msgToReturn = "CDOK"
    except:
      msgToReturn = "NOCD"
  return msgToReturn

########################################################
# Ouverture du fichier "identifiant.txt" en bas niveau #
########################################################
bdd = "identifiant.txt"
try :
  fd = os.open(bdd,os.O_RDONLY) # Ouvre le fichier en read-only
  dict = {}
  temp = ""
  c = os.read(fd,1)
  while(len(c)>0):
    if(c == b" "): # S'il y a un espace on stocke le login en tant que clé
      c = os.read(fd,1)
      dict[temp] = ""
      who = temp
      temp = ""
    if(c == b"\n"): # S'il y a un retour à la ligne on stocke le mdp en tant que valeur de la clé précédente enregistré
      dict[who] = temp
      temp = ""
    else: # Ecrit dans "temp" caractère par caractère
      temp = temp + c.decode("UTF-8")
    c = os.read(fd,1) # Lit le prochain caractère
  print(dict)  
except OSError:
  print("Ouverture d'ouverture")

####################################
# Création fils, serveur concurent #
####################################
try:
  while True:
    connobj, client = socket.accept()
    child = os.fork()
    if (child == 0) :
      socket.close()
      print("{} connected".format( client ))

      # BONJ
      response = (connobj.recv(255)).decode("UTF-8")
      if (response == "BONJ") :
        print(response+" reçu")

        i=0
        while(i<3):
          # WHO
          print("WHO envoyé")
          connobj.send(b"WHO")
          who = (connobj.recv(255)).decode("UTF-8")
          if (len(who) != 0):
            print("WHO reçu : "+who)

            # PASSWD
            print("PASSWD envoyé")
            connobj.send(b"PASSWD")
            passwd = (connobj.recv(255)).decode("UTF-8")
            if (len(passwd) != 0):
              print("PASSWD reçu : "+passwd)

              if(who in dict.keys()):
                if(dict[who]==passwd):
                  print("WELC envoyé")
                  connobj.send(b"WELC")
                  procFils = os.fork()
                  while True:
                    ### ICI METTRE CODE FONCTION DISTANTE
                    if(procFils == 0):
                      commandeRecu = (connobj.recv(255).decode("UTF-8"))
                      if(commandeRecu == "rls"):
                        print("request rls command")
                        command_result = rls_command()
                        resultToBeSent = command_result.encode()
                        connobj.send(resultToBeSent)
                      elif(commandeRecu == "rpwd"):
                        print("request rpwd command")
                        command_result = rpwd_command()
                        resultToBeSent = command_result.encode()
                        connobj.send(resultToBeSent)
                      elif(commandeRecu == "rcd"):
                        print("request rcd command")
                        path = (connobj.recv(255).decode("UTF-8"))
                        command_result = rcd_command(path)
                        resultToBeSent = command_result.encode()
                        connobj.send(resultToBeSent)
                      elif(commandeRecu == "dc"):
                        break
                      else:
                        break
                  ### TESTESTSETSETSETESTEST
                  break;
                else:
                  # Pas le bon password
                  i+=1
                  if (i == 3):
                    print("BYE envoyé")
                    connobj.send(b"BYE")
              else:
                # Login inexistant
                i+=1
                if (i == 3):
                  print("BYE envoyé")
                  connobj.send(b"BYE")
          else:
            sys.exit(0)
        else:
          sys.exit(0)
      sys.exit(0)

except KeyboardInterrupt: # Ferme proprement en cas de CTRL+C sauvage
  print("\nFermeture du serveur en cours")
  time.sleep(1)
  socket.close()

