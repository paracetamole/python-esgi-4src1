#!/usr/bin/python3

import socket, sys, os, time

#############
# VARIABLES #
#############

menu_actions = {}
host = "localhost"
port = int(sys.argv[1])

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((host, port))


############
# FONCTION #
############

# MENU #

def main_menu():
  print(" ")
  print("MENU :")
  print("1 - Commandes locales")
  print("2 - Commandes distantes")
  print("0 - Quitter")
  choix = input("Quel est votre choix ? ")
  exec_menu(choix)
  return

def exec_menu(choix):
  ch = choix.lower()
  if (ch == ''):
    menu_actions['main_menu']()
  else:
    try:
      menu_actions[ch]()
    except KeyError:
      print("Mauvais choix, try again.\n")
      menu_actions['main_menu']()
  return

def do_ls():
  pFils = os.fork()
  if(pFils):
    os.waitpid(pFils, 0)
  else:
    os.execlp('ls','ls','-a')

def do_pwd():
  pFils = os.fork()
  if(pFils):
    os.waitpid(pFils, 0)
  else:
    os.execlp('pwd','pwd',)

def do_cd():
    userCD = input("Dans quel dossier voulez-vous aller ? ")
    try:
      os.chdir(userCD)
      print(userCD)
    except:
      print("Le fichier ou dossier n'existe pas")

def menu_cmd_local():
  print("Bienvenue dans le menu des commandes locales")
  print("1 - ls")
  print("2 - pwd")
  print("3 - cd")
  print("0 - Back")
  cmd = input("Votre choix ? ")
  if (cmd == "0"):
    back()
  elif (cmd == "1"):
    do_ls()
  elif (cmd == "2"):
    do_pwd()
  elif (cmd == "3"):
    do_cd()
  else:
    print("Mauvaise touche, cest triste")
    time.sleep(1)
    os.system('clear')
    menu_cmd_local()
  main_menu()

def menu_cmd_distant():
  print("Bienvenue dans le menu des commandes distantes")
  print("1 - rls")
  print("2 - rpwd")
  print("3 - rcd")
  print("9 - disconnect")
  print("0 - Back")
  cmd = input("Votre choix ? ")
  if (cmd == "0"):
    back()
  elif (cmd == "1"):
    rls = "rls".encode()
    socket.send(rls)
    print("commande rls send")
    reponseCmd = (socket.recv(255)).decode("UTF-8")
    print(reponseCmd)
  elif (cmd == "2"):
    rpwd = "rpwd".encode()
    socket.send(rpwd)
    reponseCmd = (socket.recv(255)).decode("UTF-8")
    print(reponseCmd)
  elif (cmd == "3"):
	# faire commande cd distant
    rcd = "rcd".encode()
    socket.send(rcd)
    userCD = input("Dans quel dossier voulez-vous aller ? ")
    userCD = userCD.encode()
    socket.send(userCD)
    reponseCmd = (socket.recv(255)).decode("UTF-8")
    print(reponseCmd)
  elif (cmd == "9"):
    dc = "dc".encode()
    socket.send(dc)
    print("deconnexion")
    exit()
  else:
    print("Mauvaise touche, cest triste")
    time.sleep(1)
    os.system('clear')
    menu_cmd_distant()
  main_menu()

def back():
  menu_actions['main_menu']()

def exit():
  socket.close()
  sys.exit()

menu_actions = {
  'main_menu' : main_menu,
  '1' : menu_cmd_local,
  '2' : menu_cmd_distant,
  '9' : back,
  '0' : exit,
}

# CONNEXION AU SERVEUR #

def connexion_serveur():
  try :
    # Ouverture de la socket


    # Envoie de BONJ
    print("Bonjour et bienvenue sur le serveur NEVEU, avec le port n°{}".format(port))
    bonj = "BONJ".encode()
    socket.send(bonj)

    reponse = (socket.recv(255)).decode("UTF-8") # Premier reponse en WHO
    while(reponse != "WELC" and reponse != "BYE"):
      if (reponse == "WHO"):
        # Envoie du login (WHO)
        who = (input("Quel est ton identifiant ? ")).encode()
        socket.send(who)

        # Envoie du mot de passe (PASSWD)
        reponse = (socket.recv(255)).decode("UTF-8")
        if (reponse == "PASSWD"):
          passwd = (input("Quel est ton mot de passe ? ")).encode()
          socket.send(passwd)
          reponse = (socket.recv(255)).decode("UTF-8")

    if(reponse == "BYE"):
      print("Trop de mauvaise tentative")
      socket.close()

    if(reponse == "WELC"):
      time.sleep(1)
      print("Connexion autorisé")
      main_menu()

  except KeyboardInterrupt :
    print("\nArret de la session")
    time.sleep(1)
    pass

########
# MAIN #
########

connexion_serveur()
#main_menu()
